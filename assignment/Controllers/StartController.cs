﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using assignment.Models;
namespace assignment.Controllers
{
    public class StartController : Controller
    {
        assignmentdb1Entities db = new assignmentdb1Entities();
        //
        // GET: /Start/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Insert()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Insert(inserttb tb)
        {
            db.inserttbs.Add(tb);
            db.SaveChanges();
            ViewBag.mess = "inserted success";
            return View();
        }
        public ActionResult ViewRecord()
        {
            return View(db.inserttbs.ToList());
        }
        public ActionResult delete(int[] chkid)
        {
            foreach (int id in chkid)
            {
                var findd = db.inserttbs.Where(x => x.id == id).FirstOrDefault();
                var delete = db.inserttbs.Remove(findd);

            }
            db.SaveChanges();
            return RedirectToAction("ViewRecord");
        }
        public ActionResult select(int id)
        {
            return View(db.inserttbs.Where(x => x.id == id).ToList());
        }
        [HttpPost]
        public ActionResult select(int id, string username, string lastname, string emailaddress, int age)
        {
            var up = db.inserttbs.Where(x => x.id == id).FirstOrDefault();
            up.username = username;
            up.lastname = lastname;
            up.emailaddress = emailaddress;
            up.age = age;
            db.SaveChanges();

            return RedirectToAction("ViewRecord");
        }

	}
}